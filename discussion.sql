--------------------CLI: MARIA DB--------------------------
-- List down the Databases in the DBMS
SHOW DATABASES;

-- Create a Database
CREATE DATABASE music_db;

-- Drop (delete) a Database
DROP DATABASE music_db;

-- Select a Database
USE music_db;



--------------------PHP MY ADMIN--------------------------

-- Create tables
CREATE TABLE users( --pluralized table name
	id INT NOT NULL AUTO_INCREMENT, --not null means "required"
	username VARCHAR(50) NOT NULL,--varchar means string
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50), --optional data
	address VARCHAR(50),--optional data
	PRIMARY KEY (id)
);

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id --special rule for albums table: "foreign key" for "albums" table and column "artist_id"
		FOREIGN KEY (artist_id) REFERENCES artists(id) --assign "artist_id" as foreign key then use "artists" table, column "id" as reference for validation of artist_id
		ON UPDATE CASCADE --cascade means if reference id is changed, it will automatically update other data that is referenced to it
		ON DELETE RESTRICT --restrict means before you can delete "artist_id" in artists table, you have to delete first all data that is referenced to it
);



CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

--LINKING TABLE--
CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY(id),

	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);